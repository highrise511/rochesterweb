'use strict';

angular.module('myApp.locations', ['ngRoute'])

//.config(function($routeProvider) {
//  $routeProvider.when('/locations', {
//    templateUrl: 'locations/locations.html',
//    controller: 'locationsCtrl'
//  });
//})

.controller('locationsCtrl', function($scope) {
  var init = function() {
    topFunction();
    $scope.locationData = locationData;
    $scope.locationTypes = locationTypes;
    $scope.openFlowerModal = openFlowerModal;
  }
  var locationData = [
    {
      name:'Huckleberry Soda',
      thumbUrl:'http://www.annunakigenetics.com/uploads/1/2/8/6/12868055/img-3269_orig.jpg',
      description:'ChangeMe',
      type:'ind'
    },
    {
      name:'Blackberry Trainwreck',
      thumbUrl:'http://i45.tinypic.com/258xhmp.jpg',
      description:'ChangeMe',
      type:'ind'
    },
    {
      name:'Prism Soda',
      thumbUrl:'http://www.annunakigenetics.com/uploads/1/2/8/6/12868055/img-0730_orig.jpg',
      description:'ChangeMe',
      type:'hyb'
    },
    {
      name:'Strawberry Mamba',
      thumbUrl:'http://www.annunakigenetics.com/uploads/1/2/8/6/12868055/img-2158_orig.jpg',
      description:'ChangeMe',
      type:'hyb'
    },
    {
      name:'Black Cherry Soda',
      thumbUrl:'http://www.annunakigenetics.com/uploads/1/2/8/6/12868055/img-0730_orig.jpg',
      description:'ChangeMe',
      type:'hyb'
    },
    {
      name:'Purple Pursuassion',
      thumbUrl:'http://www.annunakigenetics.com/uploads/1/2/8/6/12868055/302070_orig.jpg',
      description:'ChangeMe',
      type:'sat'
    },
    {
      name:'Pink Lemon Aid',
      thumbUrl:'http://www.annunakigenetics.com/uploads/1/2/8/6/12868055/183374_orig.jpg',
      description:'ChangeMe',
      type:'sat'
    },
    {
      name:'Green Crack',
      thumbUrl:'https://d3atagt0rnqk7k.cloudfront.net/wp-content/uploads/2017/06/20151449/greencrack-960x600.jpg',
      description:'ChangeMe',
      type:'sat'
    },
    {
      name:'Sour Cyclone',
      thumbUrl:'http://www.annunakigenetics.com/uploads/1/2/8/6/12868055/img-0839_orig.jpg',
      description:'ChangeMe',
      type:'cbd'
    }
  ];
  var locationTypes = [
    {cd:'hyb', desc:'Hybrid'},
    {cd:'ind', desc:'Indica / Dominant'},
    {cd:'sat', desc:'Sativa / Dominant'},
    {cd:'cbd', desc:'CBD'},
  ];
  var openFlowerModal = function(element) {
    document.getElementById("locationImg").src = element.currentTarget.src;
    document.getElementById("locationModal").style.display = "block";
  }
  init();
});