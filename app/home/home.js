'use strict';
/* @ngInject */
angular.module('myApp.home', [])

//.config(function($routeProvider) {
//  $routeProvider.when('/home', {
//    templateUrl: 'home/home.html',
//    controller: 'HomeCtrl',
//    resolve: {
//      init: ['$timeout', function($timeout) {
//        topFunction();
//        var slideIndex = 0;
//        var triesLeft = 10;
//        var init = function() {
//          var x = document.getElementsByClassName("mySlides");
//          for (var i = 2; i < x.length; i++) {
//            x[i].style.display = "none"; 
//          }
//          if(x.length > 0) {
//            x[0].style.display = "block";
//            $timeout(carousel, 0);
//          }else{
//            triesLeft --;
//            if(triesLeft > 0){
//              $timeout(init, 500);
//            }
//          }
//        };
//
//        var carousel = function() {
//          var x = document.getElementsByClassName("mySlides");
//          if(x.length > 0) {
//            slideIndex++;
//            if (slideIndex >= x.length) {slideIndex = 0;}
//            x[slideIndex].style.display = "block";
//            var csi = slideIndex;
//            $timeout(function(){
//              for (var i = 0; i < x.length; i++) {
//                if(i != csi){
//                  x[i].style.display = "none"; 
//                }
//              }
//            }, 3950);
//          }
//          $timeout(carousel, 4000); // Change image every 2 seconds
//        };
//        init();
//      }]
//    }
//  });
//})

.controller('HomeCtrl', function($scope, $timeout) {
  var slideIndex = 0;
  var triesLeft = 10;
  var init = function() {
    var x = document.getElementsByClassName("mySlides");
    for (var i = 1; i < x.length; i++) {
      x[i].style.display = "none"; 
    }
    if(x.length > 0) {
//      x[0].style.display = "block";
      $timeout(carousel, 0);
    }else{
      triesLeft --;
      if(triesLeft > 0){
        $timeout(init, 500);
      }
    }
  };

  var carousel = function() {
    var x = document.getElementsByClassName("mySlides");
    if(x.length > 0) {
      slideIndex++;
      if (slideIndex >= x.length) {slideIndex = 0;}
      x[slideIndex].style.display = "block";
      var csi = slideIndex;
      $timeout(function(){
        for (var i = 0; i < x.length; i++) {
          if(i != csi){
            x[i].style.display = "none"; 
          }
        }
      }, 3950);
    }
    var timer = $timeout(carousel, 4000); // Change image every 2 seconds
    $scope.$on("$destroy", function() {
        if (timer) {
            $timeout.cancel(timer);
        }
    });
  };
  init();
})
;