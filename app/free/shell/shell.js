'use strict';

angular.module('myApp.free', [])
.controller('shellCtrl', function($scope,$timeout) {
  if(typeof(Storage) !== "undefined") {
      if (!sessionStorage.isOver21) {
        $timeout(function() {
          var el = document.getElementById('id01');
          if(el){
            el.style.display='block';
          }
        }, 500);
      } else {
//        console.log("Yes over 21...");
      }
    } else {
//        console.log("Sorry, your browser does not support web storage...");
    }
})
.directive('shell', function() {
  function link(scope, element, attrs, ctrl) {
    
  }
  return {
    restrict: 'E',
    link: link,
    transclude:true,
    controller: 'shellCtrl',
    controllerAs: 'shell',
    transclude:true,
    scope: {
    },
//    template : "<div><div ng-transclude></div><ng-show='vm.show' ng-include src='getTemplateUrl()'/></div></div>"
    templateUrl: 'free/shell/shell.html'
  };
});