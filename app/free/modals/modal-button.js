'use strict';

angular.module('myApp.mymodals', [])
.controller('modalButtonCtrl', function($scope) {
  var hideshow = function(id) {
      var modalEl = document.getElementById(id);
      if(modalEl){
        var currDisp = modalEl.style.display;
        modalEl.style.display = (currDisp === "block") ? 'none' : 'block';
      }
    }
  $scope.hideshow = hideshow;
})
.directive('modalButton', function($templateRequest, $window, $compile) {
  function link(scope, element, attrs, ctrl) {
    var myId = attrs.modalId;
    scope.show = false;
    scope.modalId=myId;
    scope.currentImage = '';
    scope.getTemplateUrl = function() {
      return "free/modals/image-modal.html";
    };
    $templateRequest(scope.getTemplateUrl()).then(function(html){
      var template = angular.element(html);
      angular.element($window.document.body).append(template);
      $compile(template)(scope);
    });
    
    element.on('click', function($event) {
      scope.hideshow(myId);
    });
  }
  return {
    restrict: 'A',
    link: link,
    transclude:true,
    controller: 'modalButtonCtrl',
    controllerAs: 'vm',
    transclude:true,
    scope: {
      modalButtonText: '@modalButtonText',
      modalTitle: '@modalTitle',
      modalImages: '@modalImages',
      modalText: '@modalText'
    },
    template : "<div ng-transclude></div>"
  };
});