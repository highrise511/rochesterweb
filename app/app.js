'use strict';
/* @ngInject */
// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ui.router',
  'myApp.home',
  'myApp.view2',
  'myApp.strains',
  'myApp.locations',
  'myApp.free',
  'myApp.mymodals',
  'myApp.maint'
])
.config(function($stateProvider, $urlRouterProvider) {
  var rootState = {
    name: 'root',
    'url': '/',
    controller: 'rootCtrl'
  }
  var homeState = {
    name: 'home',
    url: '/home',
    templateUrl: 'home/home.html',
    controller: 'HomeCtrl',
    resolve: {
      init: ['$timeout', function($timeout) {
        topFunction();
        
      }]
    }
  }
  var strainState = {
    name: 'strains',
    url: '/strains',
    templateUrl: 'strains/strains.html',
    controller: 'StrainsCtrl'
  }

  var aboutState = {
    name: 'about',
    url: '/about',
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  }
  
  var locationState = {
    name: 'locations',
    url: '/locations',
    templateUrl: 'locations/locations.html',
    controller: 'locationsCtrl'
  }

  $stateProvider.state(homeState);
  $stateProvider.state(strainState);
  $stateProvider.state(locationState);
  $stateProvider.state(aboutState);
  $urlRouterProvider.otherwise('/home');
});
//.config(function($locationProvider, $routeProvider) {
////  $locationProvider.hashPrefix('!');
//
//  $routeProvider.otherwise({redirectTo: '/home'});
//});
