'use strict';

angular.module('myApp.strains', [])

.controller('StrainsCtrl', function($scope, $timeout) {
  var slideIndex = 0;
  var triesLeft = 10;
  var init = function() {
    topFunction();
    initSlides();
    $scope.strainData = strainData;
    $scope.prodData = prodData;
    $scope.strainTypes = strainTypes;
    $scope.openFlowerModal = openFlowerModal;
    $scope.stateBase = 'strain_';
  }
  var strainData = [
    {
      cd:"hsd4",
      name:'Huckleberry Soda #4 (80% Indica)',
      thumbUrl:'http://demo.cloudimg.io/s/height/600/https://res.cloudinary.com/highrise511/image/upload/v1520219411/bud_hbsd.jpg',
      description:'Black Cherry Soda and Huckleberry Hound made the breeder’s dreams come true. The Flower is such a dark purple, it’s almost black. Its heavenly, one-of-a-kind, mixed-berry/cherry, red skittles aroma is beloved by folks that have the privilege of the experience.',
      type:'ind'
    },
    {
      cd:"stmb",
      name:'Strawberry Mamba (70% Indica)',
      thumbUrl:'http://demo.cloudimg.io/s/height/600/https://res.cloudinary.com/highrise511/image/upload/v1520219424/bud_sm2.jpg',
      description:"This is one of the most unique, beautiful, and aromatic cannabis varieties that we've ever grown. The aroma she exudes changes throughout her life. Starting with a very sharp scent that is akin to an orange peel, which then quickly evolves into a more complex strawberries-and-cream and Mamba fruit chews aroma. Mom and Pops are The Black 84’ and Pink Dream respectively.",
      type:'hyb'
    },
    {
      cd:"psm1",
      name:'Prism #1 (Sativa)',
      thumbUrl:'http://demo.cloudimg.io/s/height/600/https://res.cloudinary.com/highrise511/image/upload/v1520219435/bud_prsm.jpg',
      description:'The marvelous result of the combination of two stunning strains: Pineapple Express and Huckleberry Soda. Prism presents such gorgeous purple, pink, fuchsia, and green colors that swirl throughout the canvas of its flowers. The thick layer of trichomes adds an overall white hue to the buds; creating a look that we can only describe as a “prism of color.” It truly is a sight to behold! Prism exudes a sharp aroma of a syrupy sweet lime/pineapple/fuel terpene profile.',
      type:'sat'
    },
    {
      cd:"bcsd",
      name:'Black Cherry Soda (Indica)',
      thumbUrl:'http://demo.cloudimg.io/s/height/600/https://res.cloudinary.com/highrise511/image/upload/v1520219445/bud_bcs2.jpg',
      description:'A fruity, soda-like taste and unusually dark purple color. This indica is a heavy sedative that will taste good and make you feel even better. Make sure to grab some snacks.',
      type:'ind'
    },
    {
      cd:"prs5",
      name:'Purple Persuasion #5 (60% Sativa)',
      thumbUrl:'http://demo.cloudimg.io/s/height/600/https://res.cloudinary.com/highrise511/image/upload/v1520219462/bud_prps.jpg',
      description:'Purple Persuasion is sort of a sequel to our Pink LemonAid. The parents: Pink Lemonaid #2 and Pink Dream (Blue Dream X Huckleberry Hound). Though it is a sativa dominant, it’s so beautiful it will persuade even the most indica passionate individuals to partake.',
      type:'sat'
    },
    {
      cd:"pklm",
      name:'Pink Lemonaid (75% Sativa)',
      thumbUrl:'http://demo.cloudimg.io/s/height/600/https://res.cloudinary.com/highrise511/image/upload/v1520219474/bud_pl2.jpg',
      description:'The parents of this cross, Lemon Cheesecake and Huckleberry Hound, are exceptional strains on their own, and their chromosomes combine wonderfully to create this unique and special strain. Even a casual glance at the resin-covered, bright solid pink buds beg for a second look.',
      type:'sat'
    },
    {
      cd:"grck",
      name:'Green Crack (Sativa)',
      thumbUrl:'http://demo.cloudimg.io/s/height/600/https://res.cloudinary.com/highrise511/image/upload/v1520219484/bud_gc.jpg',
      description:'This is pure cannabis. Few strains compare to Green Crack’s sharp energy and focus as it induces an invigorating mental buzz that keeps you going throughout the day. With a tangy, fruity flavor redolent of mango.',
      type:'sat'
    },
    {
      cd:"ppe4",
      name:'Purple Pineapple Express #4 (60% Indica)',
      thumbUrl:'http://demo.cloudimg.io/s/height/600/https://res.cloudinary.com/highrise511/image/upload/v1520219506/bud_ppe4.jpg',
      description:'Welcome aboard the Purple Pineapple Express that has turned out to be an absolute powerhouse! PPE inherited plenty of purple from the the Huckleberry gene pool, as well as profuse resin production, and rich fusions of tropical fruits, coffee, and pine aromas of Pineapple Express. #4 is so densely-packed with oily resin heads that the trichomes appear to have trichomes!',
      type:'ind'
    },
    {
      cd:"srcy",
      name:'Sour Cyclone CBD (70% Indica)',
      thumbUrl:'http://demo.cloudimg.io/s/height/600/https://res.cloudinary.com/highrise511/image/upload/v1520219495/bud_sc2.jpg',
      description:'Sour Tsunami is an exceptionally hardy and easy plant to grow. It is incredibly sturdy, vigorous, resinous, and high-yielding. Pair her with a colorful male, Pink Dream. This pheno exhibits uniformity in plant structure, bud appearance, and aroma. Even more important, the independent laboratory test results reveal an ideal CBD:THC ratios - 3:1.',
      type:'cbd'
    },
    {
      cd:"bbtw",
      name:'Blackberry Trainwreck (50/50)',
      thumbUrl:'http://demo.cloudimg.io/s/height/600/https://res.cloudinary.com/highrise511/image/upload/v1520219516/bud_bbtw.jpg',
      description:'A true hybrid strain parented by two notable varieties, Blackberry Kush and Trainwreck. Dense conic buds that offer a sweet and earthy mix of berry flavors.',
      type:'hyb'
    },
    {
      cd:"spog",
      name:'Superman OG (Heavy Indica)',
      thumbUrl:'http://demo.cloudimg.io/s/height/600/https://res.cloudinary.com/highrise511/image/upload/v1520219528/bud_sog.jpg',
      description:'The product of Tahoe OG and Bubba Kush, a super heavy Indica that is sure to put you inside those couch cushions. Following a long line of kush genetics, Superman OG provides a strong heavy body sensation and will put you to sleep in a hurry. While great for pain management, muscle spasms, and insomnia, The aroma of this indica is nearly as strong as its effects. Featuring a strong skunk-like scent and piney taste, Superman OG is extremely pungent and not for the novice.',
      type:'ind'
    }
  ];
  var strainTypes = [
    {cd:'hyb', desc:'Hybrid', class:'w3-green'},
    {cd:'ind', desc:'Indica / Dominant', class:'w3-blue'},
    {cd:'sat', desc:'Sativa / Dominant', class:'w3-red'},
    {cd:'cbd', desc:'CBD', class:'w3-purple'},
  ];
  var prodData = [
    {
      name:'Caviar',
      thumbUrl:'http://emergimuv.com/rc-test/imgs/caviarballs.jpg',
      description:'',
      cls:"bg-caviarball"
    },
    {
      name:'Flower',
      thumbUrl:'http://emergimuv.com/rc-test/imgs/rf-jar-02.jpg',
      description:'',
      cls:"bg-rc-jar-02"
    },
    {
      name:'Extract',
      thumbUrl:'http://emergimuv.com/rc-test/imgs/oily.jpg',
      description:'',
      cls:"bg-oily"
    },
    {
      name:'Pre Rolls',
      thumbUrl:'http://emergimuv.com/rc-test/imgs/doublejointed.jpg',
      description:'',
      cls:"bg-doublejointed"
    }
  ];
  var strainTypes = [
    {cd:'hyb', desc:'Hybrid', class:'w3-green'},
    {cd:'ind', desc:'Indica / Dominant', class:'w3-blue'},
    {cd:'sat', desc:'Sativa / Dominant', class:'w3-red'},
    {cd:'cbd', desc:'CBD', class:'w3-purple'},
  ];
  var openFlowerModal = function(element) {
    document.getElementById("strainImg").src = element.currentTarget.src;
    document.getElementById("strainModal").style.display = "block";
  }
  
  var initSlides = function() {
    var x = document.getElementsByClassName("mySlides");
    
    for (var i = 1; i < x.length; i++) {
      x[i].style.display = "none"; 
    }
    if(x.length > 0) {
//      x[0].style.display = "block";
      $timeout(carousel, 0);
    }else{
      triesLeft --;
      if(triesLeft > 0){
        $timeout(initSlides, 500);
      }
    }
  };
  var carousel = function() {
    var x = document.getElementsByClassName("mySlides");
    if(x.length > 0) {
      slideIndex++;
      if (slideIndex >= x.length) {slideIndex = 0;}
      x[slideIndex].style.display = "block";
      var csi = slideIndex;
      $timeout(function(){
        for (var i = 0; i < x.length; i++) {
          if(i != csi){
            x[i].style.display = "none"; 
          }
        }
      }, 3950);
    }
    var timer = $timeout(carousel, 4000); // Change image every 2 seconds
    $scope.$on("$destroy", function() {
        if (timer) {
            $timeout.cancel(timer);
        }
    });
  };
  init();
});