var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var ngAnnotate = require('gulp-ng-annotate');

//script paths
var jsFiles = [
      '!app/rf.js',
      '!app/rf.min.js',
      '!app/**/*test.js',
      '!app/gulpfile.js',
      '!app/package.js',
      '!app/bower_components/**/*.min.js',
      '!app/bower_components/**/index.js',
      '!app/bower_components/**/*.config.js',
      '!app/bower_components/angular-ui-router/**/*.js',
      '!app/bower_components/angular-mocks/**/*Mock*.js',
      '!app/bower_components/less/**/browser.js',
      '!app/bower_components/html5-boilerplate/**/*.js',
      'bower_components/html5-boilerplate/dist/js/vendor/modernizr-2.8.3.min.js',
      'app/bower_components/**/*.js',
      'app/**/*.js'
    ],
    jsMinDest = 'build',
    jsDest = 'app',
    lessFiles = [
      '!app/rf.less',
      '!app/bower_components/**/*.less',
      'app/**/*.less'
    ],
    lessMinDest = 'build',
    lessDest = 'app',
    cssFiles = [
      '!app/rf.css',
//      "app/bower_components/components-font-awesome/css/font-awesome.min.css",
      "app/bower_components/w3/index.css"
    ],
    cssMinDest = 'build',
    cssDest = 'app';

gulp.task('default', ['build-js','build-less', 'build-css','watch-js', 'watch-less','watch-css']);
gulp.task('build', ['build-js-min','build-less', 'build-css']);
gulp.task('watch-js', function(){
  gulp.watch(jsFiles, ['build-js']);
});
gulp.task('watch-less', function(){
  gulp.watch(lessFiles, ['build-less']);
});
gulp.task('watch-css', function(){
  gulp.watch(cssFiles, ['build-css']);
});
gulp.task('build-js', function() {
    return gulp.src(jsFiles)
        .pipe(concat('rf.js'))
        .pipe(gulp.dest(jsDest));
});
gulp.task('build-js-min', function() {
    return gulp.src(jsFiles)
        .pipe(concat('rf.min.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest(jsDest));
});
gulp.task('build-less', function() {
    return gulp.src(lessFiles)
        .pipe(concat('rf.less'))
        .pipe(gulp.dest(lessDest))
        .pipe(rename('rf.min.less'))
        .pipe(gulp.dest(lessMinDest));
});
gulp.task('build-css', function() {
    return gulp.src(cssFiles)
        .pipe(concat('rf.css'))
        .pipe(gulp.dest(cssDest))
        .pipe(rename('rf.min.css'))
        .pipe(gulp.dest(cssMinDest));
});